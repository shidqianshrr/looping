<html>
    <head>
    <title>Foreach</title>
    <style>
        table, th, td {
        border: 1px solid black;
    }
</style>
    </head>
    <body>
        <?php
        $month = Array('Januari', 'Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        ?>
        <select name="month">
            <?php
            foreach ($month as $x => $hasil){
                $month .= '<option value="' . $x . '">' . $hasil . '</option>' . "\r\n";
            }
            echo $month
            ?>
        </select>
        
        <select name="year">
            <?php
                for ($year=0; $year <=40; $year++){
            ?>
                <option>
            <?php
                    echo 2000-$year; ?></option>
                </option>
                <?php
                    }
            ?>
        </select>

        <?php
        $departement = Array('IT', 'Document Center','Finance','Marketing');
        ?>
        <select name="departement">
            <?php
            foreach ($departement as $x => $hasil){
                $departement .= '<option value="' . $x . '">' . $hasil . '</option>' . "\r\n";
            }
            echo $departement
            ?>
        </select>
            <br>
        <input type="submit" value="submit">
        <br></br>

        <?php
         $employees = Array ( 
    0 => Array ( 
     'id' => '1',
     'name' => 'Mohammad budi kurniawan' ,
     'departement' => 'IT',
     'birthday' => '' ) ,
    1 => Array ( 
     'id' => '2',
     'name' => 'Mahrizal' ,
     'departement' => 'IT',
     'birthday' => ''  ),
    2 => Array ( 
     'id' => '3',
     'name' => 'Agus Susanto' ,
     'departement' => 'IT',
     'birthday' => '' ) ,
    3 => Array ( 
     'id' => '4',
     'name' => 'Iim Iryanto' ,
     'departement' => 'IT',
     'birthday' => '' ) ,
    4 => Array ( 
     'id' => '5',
     'name' => 'Daniel Purnama Putra' ,
     'departement' => 'IT',
     'birthday' => '' ) ,
    5 => Array ( 
     'id' => '6',
     'name' => 'Danti Iswandhari' ,
     'departement' => 'IT',
     'birthday' => '' ) ,
    6 => Array ( 
     'id' => '7',
     'name' => 'Nafsirudin' ,
     'departement' => 'IT',
     'birthday' => '' ),
    7 => Array ( 
     'id' => '8',
     'name' => 'Endang Suryaningsih' ,
     'departement' => 'Document Center',
     'birthday' => '' ),
    8 => Array ( 
     'id' => '9',
     'name' => 'Dian Rahmawan' ,
     'departement' => 'Document Center',
     'birthday' => '' ) ,
    9 => Array ( 
     'id' => '10',
     'name' => 'Raka Abi' ,
     'departement' => 'Document Center',
     'birthday' => '' ) ,
    10 => Array ( 
     'id' => '10',
     'name' => 'Agung' ,
     'departement' => 'Document Center',
     'birthday' => '' ) 
   );
        ?>

        <table style="border-width: 100%">
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Departement</th>
                <th>Birthday</th>
            </tr>

            <?php 
             foreach ($employees as $row) {
            ?>
              <tr>
               <td><?php echo $row['id']; ?></td>
               <td><?php echo $row['name']; ?></td>
               <td><?php echo $row['departement']; ?></td>
               <td><?php echo $row['birthday']; ?></td>
              </tr>
            <?php
             }
            ?>
            
        </table>
    </body>
</html>