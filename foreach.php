<?php
$bulan = array (0=>'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

$opsi_bulan = '<select name="bulan">';
foreach ($bulan as $key => $value) {
	$opsi_bulan .= '<option value="' . $key . '">' . $value . '</option>' . "\r\n";
}
$opsi_bulan .= '</select>';

echo $opsi_bulan;
?>

<?php
$tahun = array (1=>'2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019', '2020',);

$opsi_tahun = '<select name="tahun">';
foreach ($tahun as $key => $value) {
	$opsi_tahun .= '<option value="' . $key . '">' . $value . '</option>' . "\r\n";
}
$opsi_tahun .= '</select>';
echo $opsi_tahun;
?>
